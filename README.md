# DIY Low Cost Auto Resuscitator (Auto Ambu Bag)

This is our version of DIY Low Cost Auto Resuscitator or commonly known as Auto Ambu Bag
This is not a medically approved device. We believe that no patient should use a DIY device. 
But when in an urgent situation when there's lack of equipment and manpower, this serves as a backup plan for survival.
No patient should left to die due to lack of any kind of equipment or resources! Open source device, 
nobody responsible for the any outcome and for the use of this device.

## Reading Reference for the Ventilation Algorithm
* [Interpreting the shape of the pressure-volume loop by Alex Yartsev](https://derangedphysiology.com/main/cicm-primary-exam/required-reading/respiratory-system/Chapter%20554/interpreting-shape-pressure-volume-loop)
* [Mechanical Ventilation- Pressure/Volume loop by criticalcarepractitioner.co.uk](https://www.criticalcarepractitioner.co.uk/mechanical-ventilation-series-pressurevolume-loop/)

## Operation Details
**ATTN: Nobody is responsible for any outcome of using this system.
Use at your own risk!**
*  For more detail of the design, send a request email to soohuey@continental.sg

This system is using Amica NodeMCU board
The MCU controls compression and release of the Ambu bag using two servo motor
This system can be connected to a computer/laptop, the presure sensor provide serial
data output that can be read by computer. It will connect to WiFi and send alarm signal
when pressure sensor sense any abnormalities. At the same time, the buzzer will sound.
There is a dedicated Device Server unit in order for the WiFi feature to work.
WiFi feature is hardcoded, so any changes of AP and paswword is not going to work.

## Bill of Material: Electronic Parts

| **Qty** | **Item** |                       **SGD** |
| --- |------|-------------------------------------------- |
| 1 | NodeMCU Board | 14.00 |
| 2 | Servo MG995 Motor | 28.00 |
| 1 | Power Supply unit 5V 5A | 20.00 |
| 1 | Pressure Sensor BMP180 | 10.00 |
| 1 | LCD 16x02 Display | 6.00 |
| 1 | I2C Adaptor for 16x02 Display | 4.00 |
| 4 | Button switches [START/STOP, UP, DOWN, SET] | 10.00 |
| 1 | Alarm Buzzer | 5.00 |
|  | Some capacitors, resistors, and NPN transistors | 10.00 |   
|  | **TOTAL:** | **107.00** |

For hobby or school purpose, parts can be available online from Singapore: [Continental Electronics](https://www.continental.sg/)

## Control Application
The AutoPress system can be controlled both manually and using phone application. 
The purpose of the phone application is to reduce the cost of hardware (instead of having a beautiful LCD screen, use your phone!)
The phone app will only connect to the system once through WiFi and then it will operate by itself, and only send alarm trigger when abnormalities detected.

<img src="https://gitlab.com/soohuey/diy-low-cost-auto-resuscitator-auto-ambu-bag/-/raw/master/readme_resources/demoV1.gif" data-canonical-src="https://gitlab.com/soohuey/diy-low-cost-auto-resuscitator-auto-ambu-bag/-/raw/master/readme_resources/demoV1.gif" width="270" height="480" />



## Schematic
![alt text](https://gitlab.com/soohuey/diy-low-cost-auto-resuscitator-auto-ambu-bag/-/raw/master/V0.png)

## System Operation:                              

1. INIT: LCD Display: Press START button 
    ```
    "STOPPED_________"
    "________________"
    ```

2. SETTING: when START button pressed
    ```
    "SET PRESSURE LVL"
    "[NUMBER]________"  <- Press UP/DOWN to increase/decrease
    ```

3. SETTING: when SET button pressed
    ```
    "SET PUMP SPEED  "
    "[NUMBER]________"  <- Press UP/DOWN to increase/decrease
    ```

4. READY: when SET button pressed
    ```
    "READY! START?   "
    "P:[____]_S:[____]"  <- Press UP/DOWN to increase/decrease
    ```

5. PUMPING: when START button pressed
    ```
    "RUNNING...[     ]"  <- This slot display pressure sensor reading
    "P:[____]_S:[____]"  <- Press UP/DOWN to increase/decrease
    ```

6. INIT: LCD Display: Press STOP button 
    ```
    "STOPPED_________"
    "________________"
    ```

7. ERRORX: LCD Display: When pressure sensor sense abnormalities, 
                        continue pump at low pressure medium speed.
                        Alarm buzzer sounded, connect to WiFi and 
                        send warning signal. 
    ```
    "ERROR!!_________"
    "________________"
    ```
